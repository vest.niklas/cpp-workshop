#include <iostream>

int main(int argc, char const *argv[]) {

    std::cout << "Program ran with " << argc << " arguments:" << std::endl;
    for (size_t i = 0; i < argc; i++) {
        std::cout << "\t" << argv[i] << std::endl;
    }

    return 0;
}
