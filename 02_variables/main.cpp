#include <iostream>
#include <string>

int main(int argc, char const *argv[]) {
    
    // ========================================================================================================
    // declaration and definition
    // ========================================================================================================

    int age;
    float height;

    std::cout << "Undefined behavior: " << age << ", " << height << ", " << std::endl;

    std::string name;
    std::cout << "Name: '" << name << "'" << std::endl;

    char c = 'k';
    char n = '5';

    std::cout << "Character: " << n << ", ASCII: " <<  (short) n << std::endl;

    bool savage = true;
    std::cout << "Savage? " << savage << std::endl;

    // ========================================================================================================
    // assignment
    // ========================================================================================================

    name = "Herbert";
    std::cout << "Name: '" << name << "'" << std::endl;

    // ========================================================================================================
    // alternative initialization
    // ========================================================================================================

    int dogs {4};
    std::string memeString {"hehe"};

    // ========================================================================================================
    // const & references
    // ========================================================================================================

    const std::string fixedVariable = "wat";
    // fixedVariable = "watwat";

    float temperature {32.5};

    std::cout << "temperature: " << temperature << std::endl;

    float& refToTemperature = temperature;
    refToTemperature = 28.0;

    std::cout << "refToTemperature: " << refToTemperature << std::endl;
    std::cout << "temperature: " << temperature << std::endl;

    const double pi = 0;
    // double &refToPi = pi;
    const double& refToPi = pi;

    const float& crefToTemperature = temperature;
    std::cout << "crefToTemperature: " << refToTemperature << std::endl;
    temperature = 29.0;
    std::cout << "crefToTemperature: " << refToTemperature << std::endl;

    // ========================================================================================================
    // auto (brroom brooooom, mehp mehp)
    // ========================================================================================================
    auto i = 3;
    auto& refToI = i;
    const auto& anotherPiRef = pi;

    return 0;
}
