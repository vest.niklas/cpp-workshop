#include <iostream>

int max(int a, int b) {
    if (a > b) {
        return a;
    } else {
        return b;
    }
}

template <typename T>
T min(T a, T b) {
    if (a < b) {
        return a;
    } else {
        return b;
    }
}

int main(int argc, char const *argv[])
{
    // works ... sadly
    std::cout << max(3.5, 3.1) << std::endl;

    // works ... actually
    std::cout << min(3.5, 3.1) << std::endl;
    return 0;
}
