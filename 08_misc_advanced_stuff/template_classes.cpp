#include <vector>
#include <iostream>
#include <optional>

template <typename T>
using ref = std::reference_wrapper<T>;

template <typename T>
class Stack {
public:

    /**
     * Adds an element to the top of the stack.
     */
    void push(T t) {
        m_data.push_back(t);
    }

    /**
     * Returns a reference to the top-most element of the stack.
     * If the stack is empty, returns `nullopt`.
     */
    std::optional<ref<T>> peek() {
        if (m_data.empty()) {
            return std::nullopt;
        } else {
            return std::optional<ref<T>>{m_data.back()};
        }
    }

    /**
     * Removes an element from the stack and returns it.
     * If the stack is empty, returns `nullopt`.
     */
    std::optional<T> pop() {
        if (m_data.empty()) {
            return std::nullopt;
        } else {
            T element = m_data.back();
            m_data.pop_back();
            return std::optional{element};
        }
    }

    /**
     * Returns true if the stack is empty, false otherwise.
     */
    bool empty() {
        return m_data.empty();
    }

private:
    std::vector<T> m_data;

};

int main(int argc, char const *argv[])
{
    // A stack of ints
    Stack<int> intStack;
    intStack.push(1);
    intStack.push(2);
    intStack.push(3);
    intStack.push(4);
    std::cout << "Top element: " << *intStack.peek() << std::endl;

    // Editing the top element
    std::optional<ref<int>> top = intStack.peek();
    if (top) {
        int& value = top.value();
        value = 10;
    }
    std::cout << "Top element: " << *intStack.peek() << std::endl;

    // A stack of strings
    Stack<std::string> stringStack;
    for (std::string s: {"This", "is", "a", "sentence"}) {
        stringStack.push(s);
    }
    while (!stringStack.empty()) {
        std::string top = stringStack.pop().value();
        std::cout << "Popped: " << top << std::endl;
    }

    return 0;
}
