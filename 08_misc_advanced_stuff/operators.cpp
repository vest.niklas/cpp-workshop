#include <iostream>
#include <type_traits>
#include <sstream>

/**
 * Abstracts units of the distance measure into one class of values.
 * Construct new values using the static methods like `Distance::from_meters(double)`!
 */
class Distance {
public:

    double meters() const {
        return m_meters;
    }

    double centimeters() const {
        return meters() * 100.0;
    }

    double kilometers() const {
        return meters() / 1000.0;
    }

    // "factories"

    static Distance from_meters(double meters) {
        Distance d;
        d.m_meters = meters;
        return d;
    }

    static Distance from_centimeters(double centimeters) {
        return Distance::from_meters(centimeters / 100.0);
    }

    static Distance from_kilometers(double kilometers) {
        return Distance::from_meters(kilometers * 1000.0);
    }

private:
    double m_meters;
};

Distance operator+(const Distance& lhs, const Distance& rhs) {
    return Distance::from_meters(lhs.meters() + rhs.meters());
}

Distance operator-(const Distance& lhs, const Distance& rhs) {
    return Distance::from_meters(lhs.meters() - rhs.meters());
}

Distance operator/(const Distance& lhs, double denominator) {
    return Distance::from_meters(lhs.meters() / denominator);
}

Distance operator*(const Distance& lhs, double factor) {
    return Distance::from_meters(lhs.meters() * factor);
}
Distance operator*(double factor, const Distance& rhs) {
    return rhs * factor;
}

std::ostream& operator<<(std::ostream& os, const Distance& distance)  {
    os << distance.meters() << " meters";
    return os;
}

int main(int argc, char const *argv[])
{
    auto d1 = Distance::from_centimeters(10);
    auto d2 = Distance::from_meters(1.5);
    std::cout << (d1 + d2) << std::endl;
    auto d3 = (d1 + d2) * 4 / 2 ;
    std::cout << d3 << std::endl;

    std::stringstream ss;
    ss << "Oi mate! " << d3;
    std::cout << ss.str() << std::endl;
    return 0;
}
