#include <iostream>

int main(int argc, char const *argv[]) {

    // ========================================================================================================
    // if-else
    // ========================================================================================================

    // if (condition) {
    //    statement
    // } else {
    //    statement
    // }

    if (argc > 5) {
        std::cout << "Lots of args here" << std::endl;
    } else if (argc == 3) {
        std::cout << "I like that number :smirk:" << std::endl;
    } else {
        std::cout << "Boring input" << std::endl;
    }

    // ========================================================================================================
    // for-loops
    // ========================================================================================================

    // for (initialization; condition; post-iteration-update) {
    //     statement
    // }

    for (size_t i = 0; i < argc; i++) {
        std::cout << "Iteration " << i << ": " << argv[i] << std::endl;
    }

    // ========================================================================================================
    // while-loops
    // ========================================================================================================

    // while (condition) {
    //     statement
    // }

    std::cout << "Keep entering integers and enter '-1' when you are done:" << std::endl;

    int sum = 0;
    int numbers = 0;
    int current;
    std::cin >> current;
    while (current != -1) {
        numbers += 1;
        sum += current;
        std::cin >> current;
    }

    if (numbers > 0) {
        std::cout << "Average: " << (sum / static_cast<float>(numbers)) << std::endl;
    }

    // ========================================================================================================
    // switch-case
    // ========================================================================================================

    // switch (numeric_expression) {
    //     case case1: statements break;
    //     case case2: statements break;
    //     ....
    //     default: statements
    // }

    int specialNumber = 3;
    switch (specialNumber) {
        case 0:
            std::cout << "Of course i know him, it's 0" << std::endl;
            break;
        case 2:
            std::cout << "Roughly eulers number" << std::endl;
            break;
        case 3:
            std::cout << "Roughly pi" << std::endl;
            break;
        default:
            std::cout << "Mate what do i care" << std::endl;
    }

    return 0;
}
