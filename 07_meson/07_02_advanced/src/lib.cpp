#include "lib.h"
#include <iostream>

namespace OurLibrary {

Person::Person(std::string name, int age) 
    : m_name{name}, m_age{age} 
{
    // "Creating person 'ooga booga'."
    std::cout << "Creating person '" << getName() << "'." << std::endl;
    Person::_numberOfInstances += 1;

    if (!isAdult()) {
        m_years_until_majority = 18 - m_age;
    }
}

int Person::getAge() {
    return m_age;
}

const std::string& Person::getName() {
    return m_name;
}

bool Person::maySauf() {
    return m_years_until_majority <= 0;
}

bool Person::isAdult() {
    return m_age > 18;
}

Person::~Person() {
    // "Dropping person 'ooga booga'."
    std::cout << "RIP '" << getName() << "'." << std::endl;
    Person::_numberOfInstances -= 1;
}

uint Person::_numberOfInstances = 0;

uint Person::numberOfInstances() {
    return _numberOfInstances;
}

void howManyAreThereNow(const std::string& message) {
    std::cout << "(" << message << ") " << Person::numberOfInstances() << std::endl;
}

void summonBennet() {

    Person bennet{"Bennet", 100000000};
    howManyAreThereNow("summonBennet");

}

}
