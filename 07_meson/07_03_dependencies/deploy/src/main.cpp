#include "lib.h"
#include <iostream>

int main(int argc, char const *argv[])
{
    OurLibrary::Suit s = OurLibrary::Suit::SPADES;
    std::cout << "My suit is number " << (int) s << " in the enum :)" << std::endl;

    OurLibrary::Point2D origin{0, 0};
    std::cout << "Point: (" << origin.x << ", " << origin.y << ")" << std::endl;

    OurLibrary::Person salam{"Salam", 80};
    OurLibrary::Person maxi{"Maximilian", 69};

    {
        using namespace OurLibrary;
        summonBennet();
        OurLibrary::Person hannah{"Hannah", 420};
        howManyAreThereNow("in { }");
    } 

    OurLibrary::howManyAreThereNow("main");
    return 0;
}
