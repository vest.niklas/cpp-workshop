#pragma once
// #ifndef LIB_H
// #define LIB_H

#include <string>

namespace OurLibrary {

enum class Suit {
    HEARTS, 
    CLUBS, 
    DIAMONDS, 
    SPADES
};

struct Point2D {
    float x;
    float y;
};

class Person {
public:

    Person(std::string name, int age);

    const std::string& getName();

    int getAge();

    bool maySauf();

    ~Person();

    static uint numberOfInstances();

private:

    bool isAdult();

    std::string m_name;

    int m_age;

    int m_years_until_majority;

    static uint _numberOfInstances;
};

void summonBennet();

void howManyAreThereNow(const std::string& message);

} // !namespace OurLibrary

// #endif
