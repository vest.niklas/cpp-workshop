#!/usr/bin/bash

function initdir() {
    cd "./$1"
    meson setup build
    cd build
    meson compile
    cd ../../
}

initdir 07_01_basic
initdir 07_02_advanced
initdir 07_03_dependencies
