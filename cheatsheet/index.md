# Main

```cpp
int main(int argc, char const *argv[]) {
//           ^                 ^
//           |                 |_ the list of arguments
//           |_ how many arguments
}
```

# Variables

```cpp
#include <string>

int main(int argc, char const *argv[]) {

    int var = 42;
//  ^   ^     ^
//  |   |     |_ initial value
//  |   |_ identifier
//  |_ datatype

    float f = 2.7f;
    double d = 6969.69696969;
    std::string word = "UWU";

    int anotherVar {43};
//                 ^
//                 |_ alternative initialization, equivalent to ... = 43
}
```

# Control Structures

## `if`, `if`-`else`, ...

```cpp
#include <iostream>

int main(int argc, char const *argv[]) {

    if (argc < 2) {
//      ^^^^^^^^
//      |_ some condition (= boolean expression)

        std::cout << "Not enough arguments" << std::endl;
    } else {
        std::cout << "Program" << argv[0] 
                  << ", Argument: " << argv[1] << std::endl;
    }

}
```

## `for`-Loop

```cpp
#include <iostream>

int main(int argc, char const *argv[]) {

    for (size_t i = 0; i < 100; i++) {
//              ^   ^  ^^^^^^^  ^^^
//              |   |  |        |_ update the loop variable 
//              |   |  |           after the loop body
//              |   |  |_ some condition (= boolean expression). 
//              |   |     loop is executed while this is true
//              |   |_ initial value for the loop variable
//              |_ loop variable


        std::cout << "The square of " << i 
                  << " is " << i * i << std::endl;
    }

}
```

## `while`-Loop

```cpp
#include <iostream>

int main(int argc, char const *argv[]) {
    
    std::cout << "Keep entering integers and enter \
'-1' when you are done:" << std::endl;

    int sum = 0;
    int numbers = 0;
    int current;
    std::cin >> current;
    while (current != -1) {
//         ^^^^^^^^^^^^^
//         |_ some condition (= boolean expression). 
//            loop is executed while this is true.

        numbers += 1;
        sum += current;
        std::cin >> current;
//      ^^^^^^^^^^^^^^^^^^^
//      |_ while-loops (should) always update the variable 
//         of their condition in the body. otherwise, this would 
//         be an endless loop.
    }

    if (numbers > 0) {
        std::cout << "Average: " 
                  << (sum / static_cast<float>(numbers)) 
                  << std::endl;
    }

}
```

## `switch`-`case`

```cpp
#include <iostream>

// assume `Suit` enum defined here

int main(int argc, char const *argv[]) {
    // uses the `Suit` enum
    Suit s = Suit::SPADES;

    switch (s) {
//          ^
//          |_ Compare the value of this expression to all 
//             the case-branches

        case Suit::CLUBS:
            std::cout << "♣" << std::endl; 
            break;
//          ^^^^^
//          |_ The `break` isn't strictly required, but leaving 
//             it out results in madness.
        case Suit::HEARTS:
            std::cout << "♥" << std::endl; 
            break;
        case Suit::SPADES:
            std::cout << "♠" << std::endl; 
            break;
        case Suit::DIAMONDS:
            std::cout << "♦" << std::endl; 
            break;
    }
}
```

# Functions

First line is often called the function _signature_ or _prototype_. We also sometimes ask "What's the interface?" when we want to
know the signature of a function or which public members a [class](#classes) has.

```cpp
    int min(int a, int b) {
//  ^   ^   ^^^^^  ^^^^^
//  |   |   |______|
//  |   |   |
//  |   |   |_ When i call this function, what arguments
//  |   |      do I need to supply?
//  |   |_ how do i call this boi?
//  |_ what will this function return?

        std::string result;
        for (int i = s.length() - 1; i >= 0; i--) {
            result.push_back(s[i]);
        }
        return result;
    }
```

# Classes {#classes}

 TODO

# Splitting Up

TODO

# Meson

TODO

# Operators

TODO

# Templates

TODO

# Rodos Dosis

TODO (refer to move guide)

# Resources

- C++ Primer by Lippman, Lajoie, Moo
- [cppreference](https://en.cppreference.com/w/)
- [isocpp Core Guidelines](https://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines)
- [Meson Manual](https://mesonbuild.com/Manual.html)
