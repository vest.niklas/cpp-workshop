#include <iostream>
#include <string>
#include <iomanip>

enum class Suit {
    HEARTS, CLUBS, DIAMONDS, SPADES
};

struct Point2D {
    float x;
    float y;
};

class Person {
public:

    // constructor
    Person(std::string name, int age) : m_name{name}, m_age{age} {
        if (!isAdult()) {
            m_years_until_majority = 18 - m_age;
        }
    }

    // similar, but different
    // Person(std::string name, int age) {
    //     m_name = name;
    //     m_age = age;
    //     if (!isAdult()) {
    //        m_years_until_majority = 18 - m_age;
    //     }
    // }

    std::string getName() {
        return m_name;
    }

    int getAge() {
        return m_age;
    }

    bool maySauf() {
        return m_years_until_majority <= 0;
    }

    // destructor
    ~Person() {
        std::cout << "RIP '" << getName() << "'." << std::endl;
    }

private:

    bool isAdult() {
        return m_age > 18;
    }

    std::string m_name;
    int m_age;
    int m_years_until_majority;

};

int main(int argc, char const *argv[])
{
    Suit s = Suit::SPADES;
    switch (s) {
        case Suit::CLUBS: std::cout << "♣" << std::endl; break;
        case Suit::HEARTS: std::cout << "♥" << std::endl; break;
        case Suit::SPADES: std::cout << "♠" << std::endl; break;
        case Suit::DIAMONDS: std::cout << "♦" << std::endl; break;
    }

    Point2D origin{0, 0};
    std::cout << "Point: (" << origin.x << ", " << origin.y << ")" << std::endl;

    Person me{"Nik", 23};
    std::cout << std::boolalpha; // "pls print true and false as `true` and `false` and not as `1` and `0`"
    std::cout << "Darf " << me.getName() << " saufen? " << me.maySauf() << std::endl;
    return 0;
}
