#include <iostream>
#include <string>

class Person {

private:
    static uint _numberOfInstances;

public:
    static uint numberOfInstances() {
        return _numberOfInstances;
    }

    Person(std::string name, int age) 
        : m_name{name}, m_age{age} 
    {
        // "Creating person 'ooga booga'."
        std::cout << "Creating person '" << getName() << "'." << std::endl;
        Person::_numberOfInstances += 1;
    }

    ~Person() {
        // "Dropping person 'ooga booga'."
        std::cout << "Dropping person '" << getName() << "'." << std::endl;
        Person::_numberOfInstances -= 1;
    }

    std::string getName() { return m_name; }
    int getAge() { return m_age; }
private:
    std::string m_name;
    int m_age;
};

// static initialization must happen outside of class body, idk why
uint Person::_numberOfInstances = 0;

void howManyAreThereNow(const std::string& message) {
    std::cout << "(" << message << ") " << Person::numberOfInstances() << std::endl;
}

void summonBennet() {

    Person bennet{"Bennet", 100000000};
    howManyAreThereNow("summonBennet");

} // end of scope of bennet

int main(int argc, char const *argv[])
{
    Person salam{"Salam", 80};
    Person maxi{"Maximilian", 69};

    {
        summonBennet();
        Person hanna{"Hanna", 420};
        howManyAreThereNow("in { }");
    } // end of "scope" of hanna

    howManyAreThereNow("main");
    return 0;
}
