#include <iostream>

// returns void, no params
void doSomething() {
    std::cout << "in doSomething()" << std::endl;
}

// returns by-value string, by-value string parameter
std::string reverse(std::string s) {
    std::string result;
    for (int i = s.length() - 1; i >= 0; i--) {
        result.push_back(s[i]);
    }
    return result;
}

// returns boolean, by-value int parameter, defined somewhere else
bool isEven(int i);

// returns int, by-const-ref string parameter and by-value char parameter
int findFirst(const std::string& s, char c) {
    if (s.empty()) {
        return -1;
    }

    int index = 0;
    while (index < s.length() && s[index] != c) {
        index += 1;
    }

    // we didn't find the char :(
    if (index >= s.length()) {
        index = -1;
    }

    return index;
}

int main(int argc, char const *argv[])
{
    doSomething();
    
    std::string disBoi{"Hello :)"};
    std::cout << disBoi << " <-> " << reverse(disBoi) << std::endl;

    std::cout << "Is 3 even? " << isEven(3) << std::endl;

    std::cout << "First index of 'o' in 'Hello du mongo': " << findFirst("Hello du mongo", 'o') << std::endl;
    std::cout << "First index of 'r' in 'L I T': " << findFirst("L I T", 'r') << std::endl;

    return 0;
}

bool isEven(int i) {
    return i % 2 == 0;
}
